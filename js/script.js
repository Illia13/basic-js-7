"use strict";

//! 1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
//! (читається однаково зліва направо і справа наліво), або false в іншому випадку.
// let inputStr = "О гомін німого";
// let result = isPalindrome(inputStr);

// function isPalindrome(str) {
//   str = str.replace(/\s/g, "").toLowerCase();
//   return str === str.split("").reverse().join("");
// }

// if (result) {
//   console.log(inputStr);
// } else {
//   console.log(`Не є паліндромом`);
// }

//! 2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або
//! дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:

// function stringLength(inputString, maxLength) {
//   return inputString.length <= maxLength;
// }

// console.log(stringLength("checked string", 20));
// console.log(stringLength("checked string", 10));

//! 3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt.
//!  Функція повина повертати значення повних років на дату виклику функцію.

function age() {
  const userAsk = prompt("Введіть дату вашу народження:");
  const today = new Date(userAsk);
  const currentYear = new Date().getFullYear();

  let age = currentYear - today.getFullYear();

  return age;
}
const userAge = age();
console.log(userAge);
